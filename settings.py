from sqlalchemy import create_engine

POSTGRESQL = {
    'engine': 'postgresql+psycopg2',
    'pool_size': 100,
    'debug': False,
    'username': 'postgres',
    'password': '12',
    'host': 'localhost',
    'port': 5432,
    'db_name': 'todo',
}

SQLALCHEMY = {
  'debug': True,
}

