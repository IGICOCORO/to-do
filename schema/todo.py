from typing import List
from pydantic import PrivateAttr, BaseModel
from sqlalchemy_pydantic_orm import ORMBaseSchema
from models.todoModel import Todo

class TodoBase(BaseModel):
    name: str
    

class TodoList(BaseModel):
    children: List[TodoBase]