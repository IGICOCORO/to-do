from sqlalchemy import Column, String, Integer
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine

Base = declarative_base()
DB_URI = "postgresql+psycopg2://postgres:12@localhost:5432/todo"

class Todo(Base):
     __tablename__ = "todo"
     id = Column(Integer, primary_key=True)
     name = Column(String(30), nullable=False)
     
     def __repr__(self) -> str:
        return f"Todo(name:{self.name})"
     
     
if __name__ == "__main__":
 engine = create_engine(DB_URI)
 Base.metadata.drop_all(engine)
 Base.metadata.create_all(engine)