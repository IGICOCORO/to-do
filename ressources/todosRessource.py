import falcon
from models import todoModel
from utils.request_parser import parse
from schema.todo import TodoBase, TodoList
from models.todoModel import Todo
from spectree import SpecTree

spec = SpecTree("falcon")

class todosResource:
    
    def on_get(self, req, resp):
        """Get all todos """
        all_todos = req.context.db_session.query(Todo).all()
        resp.media = [{'id':x.id,'name':x.name} for x in all_todos]
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        """Create todo """
        args = parse(schema=TodoBase(partial=False,
                                       exclude=['id']),
                     request=req)

        todo = Todo(**args)
        req.context.db_session.add(todo)
        req.context.db_session.commit()
        resp.media = TodoBase().dumps(todo)
        resp.status = falcon.HTTP_200