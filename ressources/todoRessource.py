import falcon
from utils.request_parser import parse
from schema.todo import TodoBase
from models.todoModel import Todo
from utils.exceptions import EntityNotExists
from spectree import SpecTree

spec = SpecTree("falcon")

class TodoResource:
    def on_get_one_todo(self, req, resp, id):
        """ Get todo by ID """
        todo = req.context.db_session.query(Todo).get(id)

        if not todo:
            raise EntityNotExists(description='todo with id=%s not exists' % id)

        resp.media =  TodoBase().dump(todo)
        resp.status = falcon.HTTP_200
    
    
    def on_patch(self, req, resp, id):
        """ Update Todo """
        args = parse(schema=TodoBase(partial=True,
                                        exclude=['id']),
                     request=req)

        todo = req.context.db_session.query(Todo).get(id)

        if not todo:
            raise EntityNotExists(description='Todo with id=%s not exists' % id)

        todo.update(**args)

        req.context.db_session.add(todo)
        req.context.db_session.commit()

        resp.media = TodoBase().dump(todo)
        resp.status = falcon.HTTP_200

    def on_delete(self, req, resp, id):
        """ Delete todo """
        todo = req.context.db_session.query(Todo).get(id)
        req.context.db_session.delete(todo)
        req.context.db_session.commit()

        resp.media = TodoBase().dump(todo)
        resp.status = falcon.HTTP_200