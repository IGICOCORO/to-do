from wsgiref import simple_server
from wsgiref.simple_server import make_server
import falcon
from ressources.api_index import ApiIndexResource
from ressources.todoRessource import TodoResource, spec
from ressources.todosRessource import todosResource
from utils.exceptions import handler
from middleware.db_session import DbSessionManager
from spectree import SpecTree
from db import Session

spec = SpecTree("falcon",title='TODO API', version='v1.0')
app = falcon.App(
    middleware=[
        DbSessionManager(Session=Session)
    ]
)

app.add_route('/api', ApiIndexResource())
app.add_route('/api/todos', todosResource())
app.add_route('/api/todos/{id}', TodoResource())


app.add_error_handler(exception=Exception, handler=handler)



if __name__ == "__main__":
    # config = uvicorn.Config("main:app", port=8000, log_level="info")
    # server = uvicorn.Server(config)
    # server.run()
    spec.register(app)
    httpd = simple_server.make_server("localhost", 8000, app)
    httpd.serve_forever()