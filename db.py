from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker
from settings import POSTGRESQL, SQLALCHEMY



engine = create_engine(
    '{engine}://{username}:{password}@{host}:{port}/{db_name}'.format(
        **POSTGRESQL
    ),
    pool_size=POSTGRESQL['pool_size'],
    echo=SQLALCHEMY['debug']
)

session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)